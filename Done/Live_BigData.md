
# 连享会直播：经济学中的大数据应用



## 缘起

     
> &emsp;     
> 大数据来袭，我们该如何应对？机会还是挑战？    
> &emsp;      
> 数据就在那儿，可我却不知从何下手……
> &emsp;      
> &emsp;        


大数据背景下，我们邀请了中山大学岭南学院的李兵副教授，与大家一起分享大数据在经济学研究中的应用。

李兵副教授团队最近在《经济研究》、《中国工业经济》等期刊发表了一系列基于大数据的经验研究。这些研究的话题都很有趣，数据的来源乍看起来有点「无厘头」，但背后却蕴含着比传统结构化数据 (商业数据库、年鉴等) 更为丰富的信息。

- 一个地区在夜间灯光的明亮程度能反映什么呢？
- 我在卫星上看着你：夜里不亮灯，我猜你的 GDP 没那么高！
- 美团告诉我：限制了流动人口，老北京们就没那么好的口福了……
- 马云说：用我们的电子商务平台吧，你的出口会增加！
- 让机器帮我读报纸，能看出经济政策的变化吗？

在这期直播里，李兵副教授将为我们分享这几篇有趣的文章是如何切入的？如何解决技术上的难题？以及如何与传统的经济学话题关联起来？

---
## 课程概览

- **听课方式：** 网络直播。报名后点击邀请码进入在线课堂收看，无需安装任何软件。支持手机、iPad、电脑等。
- **直播嘉宾**：李兵 副教授 (中山大学)
- **费用**：88 元
- **时间**：2020年2月28(周五)，19:00-21:00
- **课程咨询：** 李老师-18636102467（微信同号）

---
## 课程提要

- 经济学研究中的数据困境
- 经济学中大数据应用实例
  - **卫星遥感数据：** 灯光数据 | 植被覆盖 | 空气污染
  - **网络平台数据：** 阿里巴巴 | 亚马逊 | 大众点评
  - **行政管理数据：** 税务局 | 海关 | 银行
  - **文本数据：** 报纸 | 专利文本 | 裁判文书
- 我们折腾过的大数据
  - 《一带一路 - [夜间灯光数据](https://quqi.gblhgk.com/s/880197/gS5HFH9Zl1z6aiPk)》
  - 《不可贸易品多样性 - [大众点评](https://quqi.gblhgk.com/s/880197/Qvnc3uPTJH7pSz1j)》
  - 《电子商务平台 - [阿里巴巴](https://quqi.gblhgk.com/s/880197/pS43Vt5992JFtnoz)》
  - 《经济政策不确定性 - [EPU 指数](https://quqi.gblhgk.com/s/880197/VYGeIZZi17QU6bVi)》
- 从哪里获取数据？我们要储备哪些技术？





## 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李兵工作照120.png)


[李兵](https://lingnan.sysu.edu.cn/teacher/993)，中山大学岭南(大学)学院副教授。主要研究方向是外商直接投资、国际贸易、国际政治经济学、互联网经济学、城市经济学、应用计量经济学等。研究成果发表于《经济研究》、《世界经济》、《中国工业经济》等中文权威期刊和Review of International Economics、The Economics of Transition等SSCI英文期刊。全国万名优秀创新创业导师人才库首批入库导师，中国技术经济学会知识产权分委会理事，《经济研究》、《世界经济》、《金融研究》等期刊的匿名审稿人。“雏鹰读书会”创始人，学术顾问。

### 论文预读

下面是本期直播中将重点分享的几篇论文，大家可以预先 [「看看」](https://quqi.gblhgk.com/s/880197/7VtqlaaoEFgmFUAe)。

- **李兵**, 郭冬梅, 刘思勤. 城市规模、人口结构与不可贸易品多样性——基于“大众点评网”的大数据分析. **经济研究**, 2019, 54(01):150-164. [[PDF]](https://quqi.gblhgk.com/s/880197/Qvnc3uPTJH7pSz1j)
- 岳云嵩, **李兵**. 电子商务平台应用与中国制造业企业出口绩效——基于“阿里巴巴”大数据的经验研究. **中国工业经济**, 2018(08):97-115.  [[PDF]](https://quqi.gblhgk.com/s/880197/pS43Vt5992JFtnoz)
- **李兵**, 龚昭, 刘思勤, 李松楠. “一带一路”国家投资适宜性研究——基于夜间灯光数据的视角. **世界经济文汇**, 2019(03):18-37.  [[PDF]](https://quqi.gblhgk.com/s/880197/gS5HFH9Zl1z6aiPk)
- **李兵**，林安琪，郭冬梅. 经济政策不确定性对进口产品的异质性影响——基于中文报纸大数据文本的实证分析，**系统工程理论与实践**，2020, 已接收. [[PDF]](https://quqi.gblhgk.com/s/880197/VYGeIZZi17QU6bVi)

> 扫码在线读文献

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李兵-大数据-文献曲奇二维码.png)

&emsp;

--- 

## 报名

> #### [长按/扫码报名]：
> https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李兵-大数据-短书报名二维码.jpg)


### 扫码入群：更多分享和讨论

> 亦可直接从海报底部扫描

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李兵-大数据-群二维码2.png) &emsp; &emsp; &emsp; 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/李兵-大数据-群二维码1.png)


&emsp;

---
## 更多精彩课程 

- **E.** &#x1F4D7; [直播-直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)，主讲：连玉君，**公开课-Free**
- **D.** &#x1F535; [直播-空间计量全局模型及Matlab实现](https://lianxh.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), 主讲：范巧。
- **C.** &#x1F34E; [视频-动态面板数据模型](https://lianxh.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), 主讲：连玉君，**随时在线观看**
- **B.** &#x1F34F; [视频-实证研究设计](https://lianxh.duanshu.com/#/brief/course/5ae82756cc1b478c872a63cbca4f0a5e)，主讲：连玉君，**随时在线观看**
- **A.** [直播-文本分析与爬虫专题](https://lianxh.duanshu.com/#/brief/live/5df08d50181e48f88e911d76a79b5a20)，**2020.3.28-29， 4.4-5**，主讲：司继春，游万海。     





&emsp;

---
>## 关于我们

>[连享会](https://www.lianxh.cn) &ensp; [最新专题](https://gitee.com/arlionn/Course/blob/master/README.md) &ensp;  [直播](http://lianxh.duanshu.com) 

[![&#x1F4D7; 点我 - 更多推文](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)](https://gitee.com/arlionn/Course/blob/master/README.md)



- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **往期精彩推文**
  - 公众号推文：[分类阅读](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [工具资源](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506) | [最新专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=1&sn=c2ae29b2f9e35d7ebaa1abbc01e0fe02)
  - 知乎专栏：[推文](https://www.zhihu.com/people/arlionn/posts) | [回答](https://www.zhihu.com/people/arlionn/answers) 
- **Stata连享会 (StataChina) 公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文。常见关键词：`DID，RDD，PSM，面板，IV，合成控制法，plus，Profile, Bootstrap, 交乘项, 平方项, 工具, 软件, Sai2, gInk, Annotator, 直击面板数据, 连老师, 直播, 空间计量, 爬虫, 文本分析, 正则, Markdown幻灯片, marp, 盈余管理, MC` ……。

---

![欢迎加入Stata连享会(公众号: StataChina)](https://file.lianxh.cn/images/20191111/ec83ed2baf9c93494e4f71c9b0f5d766.png)



