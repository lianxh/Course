
&emsp;

> [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [b 站](https://space.bilibili.com/546535876) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


## 1. 最新专题课程

> [最新上线](https://gitee.com/arlionn/collections/278581) | [公开课](https://gitee.com/arlionn/collections/278579) | [专题小课](https://gitee.com/arlionn/collections/1257)

- **[CIP-面板数据因果推断](https://file.lianxh.cn/KC/lianxh_CIP.pdf)**：2022 年 10 月 15-16；22-23；29-30 日 
- **[因果推断实用计量方法](https://gitee.com/lianxh/YGqjp)**，视频课，邱嘉平教授，55 讲，每讲 15-20min  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)


>### &#x2B55; **CIP：** 面板数据因果推断——从入门到精通
- **时间：** 2022 年 10 月 15-16；22-23；29-30 日 (三个周末/每天 3 小时)
- **方式：** 网络直播 + 45 天回放 
- **授课嘉宾：** [徐轶青](https://yiqingxu.org/) (斯坦福大学)
- **授课方式：** 幻灯片+Stata16/17 实操演示，电子板书
- **全程答疑：** 20 位经验丰富的助教全程答疑，课后分享答疑文档
- **课程主页：** <https://gitee.com/lianxh/CIP>, $\ \ {\color{red}{\looparrowright}}$ [PDF课纲](https://file.lianxh.cn/KC/lianxh_CIP.pdf)，[备讲文献](https://www.jianguoyun.com/p/DfWS7NcQtKiFCBjazdMEIAA)  
- **报名链接：** <http://junquan18903405450.mikecrm.com/lgGhkTZ>

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh-CIP-006.png)](https://gitee.com/arlionn/CIP)

&emsp;

## 2. 视频专题 - 反复看

### &#x23E9; **邱嘉平教授：[因果推断实用计量方法](https://gitee.com/lianxh/YGqjp)**   
- &#x231A; 长期有效 (上架时间：2022.4.10)；软件：Stata   
- &#x2B55; [邱嘉平教授](https://profs.degroote.mcmaster.ca/ads/jiaping/) 
- &#x26FE; **答疑服务：** 15 位助教提供全程答疑，[FAQs](https://gitee.com/lianxh/YGqjp/wikis/Home)    
- &#x26FA; **课程主页**：<https://gitee.com/lianxh/YGqjp>，&#x2B55; [PDF课纲](https://file.lianxh.cn/KC/lianxh_YGqjp.pdf)  
- &#x26FD; **报名链接：** <http://junquan18903405450.mikecrm.com/Yz6Djuv>
  - 长按/扫描二维码报名：    
   ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：因果推断使用计量方法.png)

**授课嘉宾：** [邱嘉平](https://profs.degroote.mcmaster.ca/ads/jiaping/)，麦克玛斯特大学德格罗特商学院金融系终身教授 ([IDEAS](https://ideas.repec.org/e/pqi44.html), [NBER](https://www.nber.org/people/jiaping_qiu), [Google](https://xs2.dailyheadlines.cc/citations?user=meoIFeYAAAAJ&hl=zh-CN&oi=ao))；成果见诸 Journal of Financial Economics (**JFE**), Review of Financial Studies (**RFS**), Journal of Financial Quantitative and Analysis (**JFQA**), Management Science (**MS**), Accounting Review (**AR**) 等 Top 期刊。

**课程特色**：本教程是邱嘉平老师《[因果推断实用计量方法](http://product.dangdang.com/28999209.html)》（2020，上海财经大学出版社）一书的配套视频。全套课程共 55 个视频，深入浅出地讲解面板数据模型、IV 估计、匹配方法、双重差分、自选择模型和断点回归等因果推断主流计量方法，辅以 Stata 实例。
本教程旨在为读者提供一个连接计量理论与实证研究的桥梁，特色鲜明：
- **逻辑性**。以因果推断为核心，重点解读各种因果推断方法的原理和它们之间的联系及区别。
- **直观性**。少用数学公式，用直白的语言、图形和例子来理解各种方法的本质。
- **实用性**。涵盖了因果推断常用方法，着重讲解在实际应用中面临的细节和棘手问题。

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_YG-001.png)](https://gitee.com/lianxh/YGqjp)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;



## 3. 公开课/专题课

### 公开课
- 侯丹丹，Stata软件及计量基础，6 次课 x 2h，[去听课](https://lianxh.duanshu.com/#/brief/course/49c3dbf11982456db9be1b5f595285dc)，[课件](https://gitee.com/arlionn/stataopen)
- 连玉君，直击面板数据模型，1.4h，[去听课](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38)，[B站视频](https://www.bilibili.com/video/BV1oU4y187qY)，[课件](https://gitee.com/arlionn/PanelData)
- 连玉君，Stata 33 讲，15min/讲. [去听课](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0)，[B站视频](https://space.bilibili.com/546535876/channel/detail?cid=160748)，[课件](https://gitee.com/arlionn/stata101)  
- 连玉君，论文复现：why？what？How？，2h，[去听课](https://www.lianxh.cn/news/3544e941a02d8.html)，[B站视频](https://www.bilibili.com/video/BV1D54y1n7Fa)，[课件](https://gitee.com/arlionn/rep)
- 龙志能，Stata小白的取经之路，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530)，[课件](https://gitee.com/arlionn/StataBin)
- 涂冰倩，微观数据库清理经验分享，2h，[去听课](https://gitee.com/arlionn/dataclean/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE%EF%BC%9A%E6%B6%82%E5%86%B0%E5%80%A9-%E5%BE%AE%E8%A7%82%E6%95%B0%E6%8D%AE%E5%BA%93%E6%B8%85%E7%90%86%E7%BB%8F%E9%AA%8C%E5%88%86%E4%BA%AB.md#https://gitee.com/link?target=https%3A%2F%2Fwww.lianxh.cn%2Fblogs%2F16.html)，[课件](https://gitee.com/arlionn/dataclean)
- 游万海，实证分析中的数据可视化，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)，[课件](https://gitee.com/arlionn/Rplot)
- 杨海生，如何玩转空间计量？，1.5h，[去听课](https://lianxh.duanshu.com/#/brief/course/675437c0c52f4e48947531286cef1e87)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E6%9D%A8%E6%B5%B7%E7%94%9F-%E7%8E%A9%E8%BD%AC%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F.md)
- 范巧，空间计量经济学的基本框架及分析范式，[去听课](https://lianxh.duanshu.com/#/brief/course/6b294c08454042ce860e43363bbeed5b)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 范巧，空间计量建模与高质量论文撰写，[去听课](https://lianxh.duanshu.com/#/brief/course/35252658d4a046a0b0b74ceea1f8e0ee)，[课件](https://gitee.com/arlionn/SP/blob/master/%E5%85%AC%E5%BC%80%E8%AF%BE-%E8%8C%83%E5%B7%A7-%E7%A9%BA%E9%97%B4%E8%AE%A1%E9%87%8F%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%9F%BA%E6%9C%AC%E6%A1%86%E6%9E%B6%7C%E5%88%86%E6%9E%90%E8%8C%83%E5%BC%8F.md)
- 龙志能，如何高效运用搜索解决问题？，1h，[去听课](https://lianxh.duanshu.com/#/brief/course/ac14b768d2314d43a8a805205a45d3e3)
- 王昆仑，双重差分方法的新进展-交错型DID，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/9f83a4513f7f409b96e40a1bd0c79379)
- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)

### 专题小课
- 连玉君，我的特斯拉-实证研究设计，2.2h，[去听课](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)
- 连玉君，我的甲壳虫-论文精讲与重现，6h，[去听课](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[课件](https://gitee.com/arlionn/paper101)
- 连玉君，动态面板数据模型，2.2h，[去听课](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[课件](https://gitee.com/arlionn/Live/tree/master/%E8%BF%9E%E7%8E%89%E5%90%9B-%E5%8A%A8%E6%80%81%E9%9D%A2%E6%9D%BF%E6%A8%A1%E5%9E%8B)
- 游万海，Stata 数据清洗之实战操作 (第一季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，Stata 数据清洗之实战操作 (第二季)，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/23924488072b4e458ec3bb0a830b187f)，[主页](https://gitee.com/arlionn/dataclean)
- 游万海，分位数回归，2h，[去听课](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 4. 资源分享

连享会主要通过三个渠道分享实证分析经验。

> **主页**：[www.lianxh.cn](https://www.lianxh.cn)  
  - 目前已分享 1000 余篇推文，包括 Stata/R/Python 等软件使用经验，[各类](https://www.lianxh.cn/blogs.html) 计量模型，内生性、DID、RDD 专题等。
  - **人手一份：Stata 101**，[PDF下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.pdf), [HTML下载](https://file.lianxh.cn/KC/Slides/lianxh_Stata101.html) | [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
> **b 站**：<https://space.bilibili.com/546535876> 
  - 亦可在 b 站搜索关键词 **连享会**
  - 内容：浏览量超过 10 w 的公开课视频，包括 [Stata 33 讲](https://space.bilibili.com/546535876/channel/detail?cid=160748)、[直击面板数据模型](https://www.bilibili.com/video/BV1oU4y187qY)、[论文复现：why？what？How？](https://www.bilibili.com/video/BV1D54y1n7Fa) 等。
> **码云仓库**：<https://gitee.com/arlionn>
  - 连享会收集整理的代码仓库，分为 30 多个 [专题](https://gitee.com/arlionn/collections)，500+ 个 [仓库](https://gitee.com/arlionn/projects)，涉及论文复现代码、RDD，DID，Panel，SCM，Lasso 等最新方法实现代码等。
  - 用法：申请一个码云账号，然后点击仓库右上角的 **[Fork](https://gitee.com/arlionn/list#)** 按钮，将喜欢的仓库克隆到你的账号下，随后点击仓库名称右边的 $\curvearrowright$ 按钮强制同步。

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) | [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) | [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)
- [徐现祥教授-IRE-guanyuan交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)

### Papers - 学术论文复现
- [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)
- Stata论文重现：[Harvard][harvd] | [Top 期刊论文](https://ejd.econ.mathematik.uni-ulm.de/) | [JFE][jfe]  | [github][git1] 
- 学者主页：[Angrist][Ang1] | [Acemoglu][acem]  | [Levine][ross] | [Duflo][Duflo] | [Imbens](https://scholar.harvard.edu/imbens/software) | [Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

### 推文
- 推文集合：[PDF 合集](https://file.lianxh.cn/KC/lianxh_Blogs.pdf)
  - Stata：[教程](https://www.lianxh.cn/blogs/17.html) |  [资料](https://www.lianxh.cn/blogs/35.html) | [新命令](https://www.lianxh.cn/blogs/43.html) | [结果输出](https://www.lianxh.cn/blogs/22.html) | [绘图](https://www.lianxh.cn/blogs/24.html) | [数据处理](https://www.lianxh.cn/blogs/25.html) |  [程序](https://www.lianxh.cn/blogs/26.html)
  - [回归分析](https://www.lianxh.cn/blogs/32.html) |  [面板数据](https://www.lianxh.cn/blogs/20.html) | [交乘项-调节](https://www.lianxh.cn/blogs/21.html)  | [IV-GMM](https://www.lianxh.cn/blogs/38.html) | [Logit](https://www.lianxh.cn/blogs/27.html) | [空间计量](https://www.lianxh.cn/blogs/29.html) 
  - [因果推断](https://www.lianxh.cn/blogs/19.html) |  [DID](https://www.lianxh.cn/blogs/39.html) |  [RDD](https://www.lianxh.cn/blogs/40.html) |  [PSM](https://www.lianxh.cn/blogs/41.html) |  [合成控制](https://www.lianxh.cn/blogs/42.html) | [文本分析](https://www.lianxh.cn/blogs/36.html) | [机器学习](https://www.lianxh.cn/blogs/47.html)
  - [Markdown](https://www.lianxh.cn/blogs/30.html)  | [工具软件](https://www.lianxh.cn/blogs/23.html) |  [其它](https://www.lianxh.cn/blogs/33.html)

- 热门推文
  - [稳健性检验！稳健性检验！](https://www.lianxh.cn/news/32ae13ec789a1.html)
  - [安慰剂检验！安慰剂检验！](https://www.lianxh.cn/news/cf8bd8363200d.html)
  - [Stata: 面板数据模型一文读懂](https://www.lianxh.cn/news/bf27906144b4e.html)
  - [Stata：聚类调整后的标准误-Cluster-SE](https://www.lianxh.cn/news/a7a8e613b2699.html)
  - [Stata：DID入门教程](https://www.lianxh.cn/news/3849f237b6d36.html)
  - [DID偏误问题：两时期DID的双重稳健估计量(上)-drdid](https://www.lianxh.cn/news/e6ef033e13c3e.html)
  - [DID偏误问题：多时期DID的双重稳健估计量(下)-csdid](https://www.lianxh.cn/news/762e878e7063b.html)
  - [Stata+R：一文读懂精确断点回归-RDD](https://www.lianxh.cn/news/96fb6b7e847e1.html)
  - [内生性！内生性！解决方法大集合](https://www.lianxh.cn/news/224e2b4e170e4.html)
  - [连享会：论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;


## 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等



### <font color=red>New！</font> **`lianxh` 命令发布了！**    

- 随时搜索连享会推文、Stata 资源，安装命令如下：  
  &emsp; `. ssc install lianxh`  
- 使用详情参见帮助文件 (有惊喜)：   
  &emsp; `. help lianxh`

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)